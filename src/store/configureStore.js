import { compose, createStore, combineReducers, applyMiddleware } from 'redux';
import axiosMiddleware from 'redux-axios-middleware';
import timeTrackerReducer from '../duck/timeTrackerRest';
import axios from 'axios';

export default () => {
    const client = axios.create({
        baseURL: process.env.TIMETRACKER_BACKEND_URL,
        responseType: 'json'
    });

    const rootReducer = combineReducers({
        timeTracker: timeTrackerReducer
    });

    const store = createStore(
        rootReducer,
        undefined,
        compose(
            applyMiddleware(axiosMiddleware(client))
        )
    )

    return store;
};

