import React from 'react';
import { connect } from 'react-redux';

import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';

import { findByEmail } from '../duck/timeTrackerRest';
import ResultList from './ResultList';

class QueryData extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: ''
        }
    }

    handleChange = prop => event => {
        this.setState({[prop]: event.target.value});
    }

    render() {
        return (
            <Grid container justify="center" spacing={24}>
                <Grid item xs={6}>
                    <Paper style={{ width: 500, padding: 20, margin: 20 }}>
                        <Typography variant="headline" component="h3">
                            Query data
                        </Typography>
                        <TextField
                            id="name"
                            label="Email:"
                            value={this.state.email}
                            onChange={this.handleChange('email')}
                            margin="normal"
                        />
                        <br />
                        <Button
                            onClick={() => this.props.findByEmail(this.state.email)}
                            variant="raised"
                            disabled={this.props.timeTracker.loadingEmailResults || !this.state.email}>
                                Find by email
                        </Button>
                        <ResultList results={this.props.timeTracker.emailResults}/>
                    </Paper>
                </Grid>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        timeTracker: state.timeTracker
    };
}

export default connect(mapStateToProps, {
    findByEmail: findByEmail
})(QueryData);