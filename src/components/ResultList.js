import React from 'react';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import moment from 'moment';

const formatDate = (input) => {
    return moment(input).format('D MMM HH:mm');
}

const ResultList = (props) => {
    if (!props.results) {
        return null;
    }

    return (
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>Email</TableCell>
                    <TableCell numeric>Start</TableCell>
                    <TableCell numeric>End</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {props.results.map((result, index) => {
                    return (
                        <TableRow key={index}>
                            <TableCell>{result.email}</TableCell>
                            <TableCell numeric>{formatDate(result.start)}</TableCell>
                            <TableCell numeric>{formatDate(result.end)}</TableCell>
                        </TableRow>
                    );
                })}
            </TableBody>
        </Table>
    );
}

export default ResultList;