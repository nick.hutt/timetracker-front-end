import React from 'react';
import { connect } from 'react-redux';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import orange from 'material-ui/colors/orange'
import red from 'material-ui/colors/red'

import QueryData from './QueryData';
import EnterNewRecord from './EnterNewRecord';

const theme = createMuiTheme({
    palette: {
        primary: orange,
        secondary: orange,
        error: red,
    },
});

class App extends React.Component {

    render() {
        return (
            <MuiThemeProvider theme={theme}>
                <div>
                    <AppBar position="static" color="default">
                        <Toolbar>
                            <Typography variant="title" color="inherit">
                                Time Tracker
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <QueryData/>
                    <EnterNewRecord/>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default App;