/**
 * Actions and reducers related to the time tracker backend.
 * 
 * Packaged together as a Duck (Redux Modular Bundle)
 * For more information see: https://github.com/erikras/ducks-modular-redux
 */
const REST_FAIL = "REST_FAIL";

const FIND_BY_EMAIL_REQUEST = "FIND_BY_EMAIL_REQUEST";
const FIND_BY_EMAIL_RECEIVE = "FIND_BY_EMAIL_RECEIVE";
export const findByEmail = (email) => {
    console.log(`Find by email: ${email}`);
    return {
        types: [FIND_BY_EMAIL_REQUEST, FIND_BY_EMAIL_RECEIVE, REST_FAIL],
        payload: {
            request: {
                url: `/records?email=${email}&length=10`,
                method: 'GET'
            }
        }
    }
}

// --data "email=john.doe@gmail.com" --data "start=28.11.2016 08:00" --data "end=28.11.2016 09:00" "http://192.168.99.100:8080/records"

const NEW_RECORD_REQUEST = "NEW_RECORD_REQUEST";
const NEW_RECORD_RECEIVE = "NEW_RECORD_RECEIVE";
export const enterNewRecord = (email, start, end) => {
    console.log(`Entering new record: ${email}, ${start}, ${end}`);

    var formData = new FormData();
    formData.append('email', email);
    formData.append('start', start);
    formData.append('end', end);

    return {
        types: [NEW_RECORD_REQUEST, NEW_RECORD_RECEIVE, REST_FAIL],
        payload: {
            request: {
                url: `/records`,
                method: 'POST',
                headers: {
                    'content-type': 'multipart/form-data'
                },
                data: formData
            }
        }
    }
}


const sessionDefaultState = {
    loadingEmailResults: false,
    emailResults: undefined,
    errorMessage: undefined,
    enteringNewRecord: false,
    enteredNewRecord: false
}

export default (state = sessionDefaultState, action) => {
    console.log('Session reducer received: ' + action.type, action);

    switch(action.type) {
        case FIND_BY_EMAIL_REQUEST:
            return {
                ...state,
                emailResults: undefined,
                loadingEmailResults: true
            };
        case FIND_BY_EMAIL_RECEIVE:
            if (action.error) {
                return {
                    ...state,
                    loadingEmailResults: false,
                    emailResults: undefined,
                    errorMessage: 'Failed to load results'
                }
            } else {
                return {
                    ...state,
                    loadingEmailResults: false,
                    emailResults: sanitizeResults(action.payload.data)
                };
            }
        case NEW_RECORD_REQUEST:
            return {
                ...state,
                enteringNewRecord: true,
                enteredNewRecord: false
            };
        case NEW_RECORD_RECEIVE:
            if (action.error) {
                return {
                    ...state,
                    enteringNewRecord: false,
                    enteredNewRecord: false
                };
            } else {
                return {
                    ...state,
                    enteringNewRecord: false,
                    enteredNewRecord: true
                };
            }
        case REST_FAIL:
            return {
                ...sessionDefaultState,
                authError: "An error occurred"
            };
        default:
            return state;
    }
}

const sanitizeResults = (input) => {
    if (!input) {
        return input;
    }

    return input.filter(input => input != null);
};