# Frontend React Starter

## Getting Started

To run the project:

```
> npm install --no-save
> npm start
```
## Docker

To build a Docker image with latest tag.
```
> docker build -t timetracker-front-end:latest .
```

To run the Docker container.
```
> docker run -p 10080:80 -e "TIMETRACKER_BACKEND_URL=http://192.168.99.100:10080/api/" -d timetracker-front-end:latest timetracker-front-end
```

You can then go to `http://192.168.99.100:10080/` to see the site. 

Prerequiste, backend should be running on 192.168.99.100:8080.
https://hub.docker.com/r/alirizasaral/timetracker/

Alternatively use docker compose.
```
> docker-compose up
```

## Info

Other starter kits can be found at:
https://reactjs.org/community/starter-kits.html