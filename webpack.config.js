const path = require('path'),
    webpack = require('webpack');
      
module.exports = {
    entry: [
        './src/index.js'
    ],
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ['react', 'es2015', 'stage-1']
            }
        },
        {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [
                'file-loader'
            ]
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    devServer: {
        historyApiFallback: true,
        contentBase: path.join(__dirname, 'public'),
        inline: true,
        port: 8090,
        proxy: {
            "/api/": {
                target: "http://192.168.99.100:8080",
                pathRewrite: {"^/api" : ""}
            } 
        }
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
            'process.env.TIMETRACKER_BACKEND_URL': JSON.stringify(process.env.TIMETRACKER_BACKEND_URL || 'http://localhost:8090/api') 
       })
    ]
};