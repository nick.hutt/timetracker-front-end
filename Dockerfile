FROM nginx:alpine

EXPOSE 80

RUN apk add nodejs curl --update \
    && rm -rf /var/cache/apk/*

COPY nginx/default.conf /etc/nginx/conf.d/

WORKDIR /tmp

COPY package.json webpack.config.js ./
COPY public ./public
COPY src ./src
RUN npm install --no-save

ENTRYPOINT npm run-script build \
    && cp -r public/* /usr/share/nginx/html/ \
    && exec nginx -g 'daemon off;'